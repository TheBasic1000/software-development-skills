# Software Development Skills | Juho Hupanen 0526260

This repo contains course work and project for course Software Develoopment Skills.

## How to run the project

Make sure you have the latest versions of MongoDB and Node.js (including nodemon).

Start mongodb.

```bash
net start MongoDB
```

Start the server running nodemon in the root directory (/project).

```bash
nodemon
```

Open localhost:3000 in your browser.

## Video of the project running
[YouTube link](https://www.youtube.com/watch?v=z0DPus69jAI)


